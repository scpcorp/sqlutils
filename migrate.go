package sqlutils

import (
	"database/sql"
	"fmt"
	"log"

	sqlmigrate "github.com/rubenv/sql-migrate"
)

const dialect = "postgres" // For sql-migrate.

// IsMigrated checks if all provided migrations are already applied.
func IsMigrated(db *sql.DB, migrations []*sqlmigrate.Migration) (bool, error) {
	records, err := sqlmigrate.GetMigrationRecords(db, dialect)
	if err != nil {
		return false, fmt.Errorf("get migration records: %w", err)
	}

	if len(records) != len(migrations) {
		return false, nil
	}
	for i := range migrations {
		// Make sure each migration from code is stored in database records.
		migrationID := migrations[i].Id
		found := false
		for _, migration := range records {
			if migrationID == migration.Id {
				found = true
				break
			}
		}
		if !found {
			// `migrationID` is missing.
			return false, nil
		}
	}

	return true, nil
}

func emptyMigrations(migrations []*sqlmigrate.Migration) []*sqlmigrate.Migration {
	empty := make([]*sqlmigrate.Migration, len(migrations))
	for i, m := range migrations {
		empty[i] = &sqlmigrate.Migration{
			Id: m.Id,
		}
	}
	return empty
}

func migrate(db *sql.DB, migrations []*sqlmigrate.Migration, emptyDB func(db *sql.DB) (bool, error)) error {
	task := &sqlmigrate.MemoryMigrationSource{}
	dbIsEmpty, err := emptyDB(db)
	if err != nil {
		return fmt.Errorf("failed to check if database is empty: %w", err)
	}
	if dbIsEmpty {
		// DB is empty, tables that should be migrated don't exist. We apply empty (mock) migrations
		// to create utility migration table and mark all migrations as applied.
		task.Migrations = emptyMigrations(migrations)
	} else {
		// Database is not empty, we need to apply migrations (i.e. tables that should be migrated exist).
		task.Migrations = migrations
	}

	// Migration itself.
	// sql-migrate promises atomic migration: https://github.com/rubenv/sql-migrate/blob/7aeab111a2a70d2be9fbf4ea2dfbf74823175115/README.md?plain=1#L15
	_, err = sqlmigrate.Exec(db, dialect, task, sqlmigrate.Up)
	if err != nil {
		return fmt.Errorf("exec migration: %w", err)
	}

	return nil
}

// Migrate checks if there are unapplied migrations and applies them in such case.
// It handles empty database as special case: no migrations are applied, but all of them are marked applied.
func Migrate(db *sql.DB, migrations []*sqlmigrate.Migration, emptyDB func(db *sql.DB) (bool, error), serviceName string) error {
	// We need to set global flag to set table name of utility migration table used by sql-migrate.
	// In our model multiple services share the database, so we use separate tables
	// for migrations of each service.
	if serviceName != "" {
		sqlmigrate.SetTable(serviceName + "_migrations")
	}

	isMigrated, err := IsMigrated(db, migrations)
	if err != nil {
		return fmt.Errorf("is migrated: %w", err)
	}

	if isMigrated {
		// Database is already migrated, nothing to do.
		return nil
	}

	// Migrate database.
	log.Println("Start migration")
	if err := migrate(db, migrations, emptyDB); err != nil {
		return fmt.Errorf("migrate: %w", err)
	}
	log.Println("Migrated successfully")
	return nil
}
