module gitlab.com/scpcorp/sqlutils

go 1.20

require github.com/rubenv/sql-migrate v1.6.1

require github.com/go-gorp/gorp/v3 v3.1.0 // indirect
